﻿using SchedulingCore.DecisiveRules;
using SchedulingCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SchedulingCore
{
    public static class SortDecisiveRules
    {
        public static void SortDetailsBatches(List<ProductBatch> productBatches, int rule)
        {
            switch (rule)
            {
                case 0:
                    AllRules.SortDetailsBatches(productBatches);
                    break;
                case 1:
                    Lukr.SortDetailsBatches(productBatches);
                    break;
                case 2:
                    InverseLukr.SortDetailsBatches(productBatches);
                    break;
                case 3:
                    Spt.SortDetailsBatches(productBatches);
                    break;
                case 4:
                    MySort.SortDetailsBatches(productBatches);
                    break;
                case 5:
                    Lpt.SortDetailsBatches(productBatches);
                    break;
                default:
                    break;
            }
            
            
        }
    }
}
