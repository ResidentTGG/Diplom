﻿using System;
using System.Collections.Generic;
using System.Text;
using SchedulingCore.Entities;
using System.Linq;

namespace SchedulingCore
{
    public static class Sorting
    {
        //Согласно теореме использую формулировки времен A,B,D,X,Z
        internal static void SortProductBatches(List<ProductBatch> productBatches)
        {
            //Ищем партию с минимальным А - временем, она идет первая
            var indexMin = FindMinAtime(productBatches, 0);
            Swap(productBatches, indexMin, 0);

            //Ищем партии Ai<=Bl && Ai-Bl->0, если таких нет,то Aj>Bl, Aj-Bl->0
            for (int l = 1; l < productBatches.Count - 1; l++)
            {
                var productBatch = (FindAiBl(productBatches, l) == null)
                                     ? FindAjBl(productBatches, l)
                                     : FindAiBl(productBatches, l);

                Swap(productBatches, productBatches.IndexOf(productBatch), l);
            }
        }
        private static ProductBatch FindAiBl(List<ProductBatch> productBatches, int l)
        {
            var sortProductBatches = new List<ProductBatch>();
            for (int i = l; i < productBatches.Count; i++)
            {
                if (CountA(productBatches[i]) <= CountB(productBatches[l - 1]))
                    sortProductBatches.Add(productBatches[i]);
            }

            if (sortProductBatches.Any())
            {
                sortProductBatches=sortProductBatches.OrderBy(pb => Math.Abs(CountZ(productBatches[l - 1], pb))).ToList();
                return sortProductBatches[0];
            }
            else
                return null;
        }

        private static ProductBatch FindAjBl(List<ProductBatch> productBatches, int l)
        {
            var sortProductBatches = new List<ProductBatch>();
            for (int i = l; i < productBatches.Count; i++)
            {
                if (CountA(productBatches[i]) > CountB(productBatches[l - 1]))
                    sortProductBatches.Add(productBatches[i]);
            }
            sortProductBatches=sortProductBatches.OrderBy(pb => Math.Abs(CountZ(productBatches[l - 1], pb))).ToList();
            return sortProductBatches[0];
        }

        internal static void SortToolOrder(List<ProductBatch> productBatches)
        {
            foreach (var productBatch in productBatches)
                foreach (var detailsBatchGroup in productBatch.DetailsBatchesGroups)
                    for (int i = 0; i < detailsBatchGroup.DetailsBatches.Count; i++)
                    {
                        var detailsBatch = detailsBatchGroup.DetailsBatches[i];
                        var machOrder = new List<int>();
                        detailsBatch.Detail.MachiningOrder.ForEach((item) => machOrder.Add(item));
                        for (int j = 0; j < detailsBatch.Detail.MachiningOrder.Count; j++)
                        {

                            if (machOrder[j] != j + 1)
                            {
                                var index = machOrder[j] - 1;
                                SwapMachOrder(machOrder, j, index);
                                SwapMachDurations(detailsBatch, j, index);
                                SwapMachStartTimes(detailsBatch, j, index);
                                SwapMachEndTimes(detailsBatch, j, index);
                                j--;
                            }
                        }

                    }
        }

        private static void SwapMachOrder(List<int> machOrder, int a, int b)
        {
            var buf = machOrder[a];
            machOrder[a] = machOrder[b];
            machOrder[b] = buf;
        }

        private static void SwapMachDurations(DetailsBatch detailsBatch, int a, int b)
        {
            var buf = detailsBatch.MachiningDurations[a];
            detailsBatch.MachiningDurations[a] = detailsBatch.MachiningDurations[b];
            detailsBatch.MachiningDurations[b] = buf;
        }

        private static void SwapMachStartTimes(DetailsBatch detailsBatch, int a, int b)
        {
            var buf = detailsBatch.StartTimes[a];
            detailsBatch.StartTimes[a] = detailsBatch.StartTimes[b];
            detailsBatch.StartTimes[b] = buf;
        }

        private static void SwapMachEndTimes(DetailsBatch detailsBatch, int a, int b)
        {
            var buf = detailsBatch.EndTimes[a];
            detailsBatch.EndTimes[a] = detailsBatch.EndTimes[b];
            detailsBatch.EndTimes[b] = buf;
        }



        private static double CountA(ProductBatch productBatch)
        {
            return productBatch.AssemblingStartTime - productBatch.MachiningStartTime;
        }

        private static double CountB(ProductBatch productBatch)
        {
            return productBatch.AssemblingEndTime - productBatch.MachiningEndTime;
        }

        private static double CountZ(ProductBatch b, ProductBatch a)
        {
            return (CountA(a) - CountB(b));
        }

        private static void Swap(List<ProductBatch> productBatches, int a, int b)
        {
            var bufProductBatch = productBatches[a];
            productBatches[a] = productBatches[b];
            productBatches[b] = bufProductBatch;
        }

        private static int FindMinAtime(List<ProductBatch> productBatches, int startIndex)
        {
            ProductBatch productBatchMin = productBatches[0];
            foreach (var productBatch in productBatches)
            {
                if ((productBatch.AssemblingStartTime - productBatch.MachiningStartTime) < (productBatchMin.AssemblingStartTime - productBatchMin.MachiningStartTime))
                    productBatchMin = productBatch;
            }
            return productBatches.IndexOf(productBatchMin);
        }
    }
}
