﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchedulingCore.Entities;
using Newtonsoft.Json;
using System.IO;

namespace SchedulingCore
{
    public static class WorkingWithFiles
    {
        const string _detailsFilePath = "A:\\Learning\\Diplom\\Files\\details.txt";
        const string _productsFilePath = "A:\\Learning\\Diplom\\Files\\products.txt";

        public static List<Detail> ReadDetailsFile()
        {
            string json = System.IO.File.ReadAllText(_detailsFilePath);
            var details = JsonConvert.DeserializeObject<List<Detail>>(json);
            return details;
        }

        public static List<FileProduct> ReadProductsFile()
        {
            string json = System.IO.File.ReadAllText(_productsFilePath);
            var products = JsonConvert.DeserializeObject<List<FileProduct>>(json);
            return products;
        }

        public static void CreateData()
        {
            var details = ReadDetailsFile();
            var products = ReadProductsFile();

            //Заполнить базу случайными числами
            RandomBase.Fill(products);
            RandomBase.Fill(details);


            //Задать число изд
            WriteProductsFile(products);
            WriteDetailsFile(details);
        }

        private static void WriteDetailsFile(List<Detail> details)
        {
            string json = JsonConvert.SerializeObject(details);
            File.WriteAllText(_detailsFilePath, json);
        }

        private static void WriteProductsFile(List<FileProduct> products)
        {
            string json = JsonConvert.SerializeObject(products);
            File.WriteAllText(_productsFilePath, json);
        }
    }
}
