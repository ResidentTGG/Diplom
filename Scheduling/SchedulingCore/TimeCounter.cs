﻿using SchedulingCore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SchedulingCore
{
    public static class TimeCounter
    {
        //Расчет продолжительности,начальных,конечных времен обработки,сборки для партий изделий 
        //Начальные времена обработки все с 0, т.к. еще не известен порядок
        internal static void CountProductBatchesTimes(List<ProductBatch> productBatches)
        {
            foreach (var productBatch in productBatches)
            {
                if (productBatch.DetailsBatchesGroups.Count == 1)
                {
                    productBatch.MachiningDuration = productBatch.Product.MachiningDuration * productBatch.CountOfProducts;
                    productBatch.MachiningStartTime = 0;
                    productBatch.MachiningEndTime = productBatch.MachiningDuration;

                    var assemblingDurations = new List<double>();
                    var assemblingStartTimes = new List<double>();
                    var assemblingEndTimes = new List<double>();
                    var machDurs = new List<double>();

                    machDurs.Add(productBatch.MachiningDuration);
                    if (productBatch.CountOfProducts == 1)

                        assemblingDurations.Add(productBatch.Product.AssemblingDuration * productBatch.CountOfProducts);

                    else if (productBatch.CountOfProducts <= productBatch.DetailsBatchesGroups[0].DetailsBatches[0].Detail.AssemblingTimes.Count)

                        assemblingDurations.Add(productBatch.Product.AssemblingDuration * 2);
                    else
                    {
                        assemblingDurations.Add(productBatch.Product.AssemblingDuration * (productBatch.CountOfProducts - productBatch.DetailsBatchesGroups[0].DetailsBatches[0].Detail.AssemblingTimes.Count));
                        if (assemblingDurations[0] < productBatch.Product.AssemblingDuration * 2)
                            assemblingDurations[0] = productBatch.Product.AssemblingDuration * 2;
                    }


                    assemblingStartTimes.Add(productBatch.Product.MachiningDuration);
                    assemblingEndTimes.Add(assemblingStartTimes[0] + assemblingDurations[0]);

                    var diff = assemblingEndTimes[0] - productBatch.MachiningEndTime;
                    if (diff < productBatch.Product.AssemblingDuration)
                    {
                        assemblingEndTimes[0] = productBatch.MachiningEndTime + productBatch.Product.AssemblingDuration;
                        assemblingStartTimes[0] = assemblingEndTimes[0] - assemblingDurations[0];
                    }
                    productBatch.AssemblingEndTime = assemblingEndTimes[0];
                    productBatch.AssemblingStartTime = assemblingStartTimes[0];
                    productBatch.AssemblingDuration = assemblingDurations[0];

                    productBatch.AssemblingDurations = assemblingDurations;
                    productBatch.AssemblingEndTimes = assemblingEndTimes;
                    productBatch.AssemblingStartTimes = assemblingStartTimes;
                    productBatch.MachiningDurations = machDurs;
                }
                else if (productBatch.GroupsCount == 1)
                {
                    productBatch.MachiningDuration = productBatch.Product.SpecialMachiningDuration;
                    productBatch.MachiningStartTime = 0;
                    productBatch.MachiningEndTime = productBatch.MachiningDuration;

                    var assemblingDurations = new List<double>();
                    var assemblingStartTimes = new List<double>();
                    var assemblingEndTimes = new List<double>();

                    assemblingStartTimes.Add(productBatch.MachiningEndTime);

                    if (productBatch.CountOfProducts == 1)
                        assemblingDurations.Add(productBatch.Product.AssemblingDuration * productBatch.CountOfProducts);
                    else if (productBatch.CountOfProducts <= productBatch.DetailsBatchesGroups[0].DetailsBatches[0].Detail.AssemblingTimes.Count)

                        assemblingDurations.Add(productBatch.Product.AssemblingDuration * 2);
                    else
                    {
                        assemblingDurations.Add(productBatch.Product.AssemblingDuration * (productBatch.CountOfProducts - productBatch.DetailsBatchesGroups[0].DetailsBatches[0].Detail.AssemblingTimes.Count));
                        if (assemblingDurations[0] < productBatch.Product.AssemblingDuration * 2)
                            assemblingDurations[0] = productBatch.Product.AssemblingDuration * 2;
                    }

                    assemblingEndTimes.Add(assemblingStartTimes[0] + assemblingDurations[0]);

                    productBatch.AssemblingEndTimes = assemblingEndTimes;
                    productBatch.AssemblingStartTimes = assemblingStartTimes;
                    productBatch.AssemblingDurations = assemblingDurations;

                    productBatch.AssemblingStartTime = productBatch.AssemblingStartTimes[0];
                    productBatch.AssemblingEndTime = productBatch.AssemblingEndTimes[0];
                    productBatch.AssemblingDuration = productBatch.AssemblingDurations[0];

                    FillMachiningDurations(productBatch);
                }
                else
                {
                    productBatch.MachiningDuration = productBatch.Product.MachiningDuration * (productBatch.GroupsCount - 1) + productBatch.Product.SpecialMachiningDuration;
                    productBatch.MachiningStartTime = 0;
                    productBatch.MachiningEndTime = productBatch.MachiningDuration;

                    var assemblingDurations = new List<double>();
                    var assemblingStartTimes = new List<double>();
                    var assemblingEndTimes = new List<double>();

                    for (int i = 0; i < productBatch.GroupsCount; i++)
                    {
                        if (i == 0)
                        {
                            assemblingStartTimes.Add(productBatch.Product.MachiningDuration);
                            assemblingDurations.Add(productBatch.Product.AssemblingDuration * productBatch.ProductsCountInBatch[0]);
                            assemblingEndTimes.Add(assemblingStartTimes[i] + assemblingDurations[i]);
                        }
                        else
                        {

                            if (i == productBatch.GroupsCount - 1)
                            {
                                assemblingStartTimes.Add(productBatch.Product.MachiningDuration * i + productBatch.Product.SpecialMachiningDuration);
                                assemblingDurations.Add(productBatch.Product.AssemblingDuration * productBatch.ProductsCountInBatch[1]);
                            }
                            else
                            {
                                assemblingStartTimes.Add(productBatch.Product.MachiningDuration * (i + 1));
                                assemblingDurations.Add(productBatch.Product.AssemblingDuration * productBatch.ProductsCountInBatch[0]);
                            }

                            assemblingEndTimes.Add(assemblingStartTimes[i] + assemblingDurations[i]);
                            var diff = assemblingStartTimes[i] - assemblingEndTimes[i - 1];
                            if (diff < 0)
                            {
                                assemblingStartTimes[i] -= diff;
                                assemblingEndTimes[i] -= diff;
                            }

                        }
                    }
                    productBatch.AssemblingEndTime = assemblingEndTimes[(int)(productBatch.GroupsCount - 1)];
                    productBatch.AssemblingStartTime = assemblingStartTimes[0];
                    productBatch.AssemblingDuration = productBatch.AssemblingEndTime - productBatch.AssemblingStartTime;
                    productBatch.AssemblingEndTimes = assemblingEndTimes;
                    productBatch.AssemblingStartTimes = assemblingStartTimes;
                    productBatch.AssemblingDurations = assemblingDurations;
                    FillMachiningDurations(productBatch);
                }

            }
        }

        private static void FillMachiningDurations(ProductBatch productBatch)
        {

            var machDurations = new List<double>();
            for (int i = 0; i < productBatch.AssemblingStartTimes.Count; i++)
            {

                if (i == productBatch.GroupsCount - 1)
                    machDurations.Add(productBatch.Product.SpecialMachiningDuration);
                else
                    machDurations.Add(productBatch.Product.MachiningDuration);
            }
            productBatch.MachiningDurations = machDurations;

        }

        internal static void CountProductsBatchesCost(List<ProductBatch> productBatches)
        {
            for (int i = 0; i < productBatches.Count; i++)
            {
                if (productBatches[i].DetailsBatchesGroups.Count == 1)
                    productBatches[i].Cost = productBatches[i].CountOfProducts * productBatches[i].Product.Cost;
                else
                    productBatches[i].Cost = (productBatches[i].GroupsCount - 1) * productBatches[i].Product.Cost;
            }
        }

        //Считает время обработки для одного изделия
        public static void CountProductMachiningTime(List<ProductBatch> productBatches)
        {
            foreach (var productBatch in productBatches)
            {
                double diff = 0;

                var startTime = FindMinStartTime(productBatch.DetailsBatchesGroups[0].DetailsBatches);
                var endTime = FindMaxEndTime(productBatch.DetailsBatchesGroups[0].DetailsBatches);
                diff = endTime - startTime;

                productBatch.Product.MachiningDuration = diff;

                if (productBatch.DetailsBatchesGroups.Count == 2)
                {
                    diff = 0;
                    startTime = FindMinStartTime(productBatch.DetailsBatchesGroups[1].DetailsBatches);
                    endTime = FindMaxEndTime(productBatch.DetailsBatchesGroups[1].DetailsBatches);
                    diff = endTime - startTime;
                    productBatch.Product.SpecialMachiningDuration = diff;
                }

            }
        }

        private static double FindMaxEndTime(List<DetailsBatch> detailsBatches)
        {
            var maxs = new List<double>();
            for (int i = 0; i < detailsBatches.Count; i++)
                maxs.Add(detailsBatches[i].EndTimes.Max());
            return maxs.Max();
        }

        private static double FindMinStartTime(List<DetailsBatch> detailsBatches)
        {
            var detailsBatch = detailsBatches[0];
            return detailsBatch.StartTimes.Min();
        }

        //Считаются продолжительности обработки для партии одного типа детали
        //Начальные и конечные времена считаются,начиная с 0
        internal static void CountMachiningTimes(DetailsBatch detailsBatch)
        {
            var durations = new List<double>();
            var delaysAfterLastTool = new List<double>();
            var startTimes = new List<double>();
            var endTimes = new List<double>();

            for (int i = 0; i < detailsBatch.Detail.MachiningOrder.Count; i++)
            {

                var delayAfterLastTool = (i == 0) ? 0 : (detailsBatch.Detail.MachiningTimes[i - 1] + detailsBatch.Detail.ReadjustmentTimes[i - 1]);
                delaysAfterLastTool.Add(delayAfterLastTool);

                double duration;
                duration = detailsBatch.CountOfDetails * detailsBatch.Detail.MachiningTimes[i] + detailsBatch.Detail.ReadjustmentTimes[i];

                if (i == 0)
                {
                    startTimes.Add(0);
                    endTimes.Add(duration);
                }
                else
                {
                    int j = i;
                    while (j != 0 && durations[j - 1] == 0)
                        j--;
                    if (j == 0)
                        j = 1;

                    var startTime = startTimes[j - 1] + delayAfterLastTool;
                    double endTime;
                    if ((startTime + duration) > endTimes[j - 1] + detailsBatch.Detail.MachiningTimes[i])
                        endTime = startTime + duration;
                    else
                    {
                        endTime = endTimes[j - 1] + detailsBatch.Detail.MachiningTimes[i];
                        startTime = endTime - duration;
                    }

                    if (detailsBatch.Detail.MachiningTimes[i] == 0)
                    {
                        endTime = endTimes[j - 1];
                        duration = 0;
                        startTime = endTimes[j - 1];
                    }
                    endTimes.Add(endTime);
                    startTimes.Add(startTime);
                }
                durations.Add(duration);

            }
            detailsBatch.MachiningDurations = durations;
            detailsBatch.EndTimes = endTimes;
            detailsBatch.StartTimes = startTimes;
            detailsBatch.DelaysAfterLastTool = delaysAfterLastTool;
        }

        //Подсчет времени сборки для каждого типа изделия
        internal static void CountAssemblingTime(List<Product> products)
        {
            foreach (var product in products)
            {
                double time = 0;
                for (int i = 0; i < product.Details.Count; i++)
                {
                    for (int j = 0; j < product.Details[i].AssemblingTimes.Count; j++)
                        time += (product.Details[i].AssemblingTimes[j] * product.DetailsCount[i]);
                }
                product.AssemblingDuration = time;
            }
        }
    }


}
