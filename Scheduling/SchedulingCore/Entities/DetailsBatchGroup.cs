﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SchedulingCore.Entities
{
    public class DetailsBatchGroup
    {
        public List<DetailsBatch> DetailsBatches { get; set; }
    }
}
