﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchedulingCore.Entities
{
    public class Product
    {
        

        public int Id { get; set; }
        public string Name { get; set; }
        public List<Detail> Details { get; set; }
        public List<int> DetailsCount { get; set; }
        public double AssemblingDuration { get; set; }
        public double MachiningDuration { get; set; }
        public double Cost { get; set; }
        public double SpecialMachiningDuration { get; set; }
    }
}
