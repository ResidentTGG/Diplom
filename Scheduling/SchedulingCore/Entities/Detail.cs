﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchedulingCore.Entities
{
    public class Detail
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<int> MachiningOrder { get; set; }
        public List<double> MachiningTimes { get; set; }
        public List<double> ReadjustmentTimes { get; set; }
        public double ReadjustmentCost { get; set; }
        public List<double> AssemblingTimes { get; set; }
    }
}
