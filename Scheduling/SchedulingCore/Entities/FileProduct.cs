﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchedulingCore.Entities
{
    public class FileProduct
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<int> DetailIds { get; set; }
        public List<int> DetailsCount { get; set; }
    }
}
