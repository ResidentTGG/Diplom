﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchedulingCore.Entities
{
    public class ProductBatch
    {
        public Product Product { get; set; }
        public int CountOfProducts { get; set; }
        public List<DetailsBatchGroup> DetailsBatchesGroups { get; set; }
        public double MachiningStartTime { get; set; }
        public double MachiningEndTime { get; set; }
        public double MachiningDuration { get; set; }
        public List<double> MachiningDurations { get; set; }
        public List<double> AssemblingStartTimes { get; set; }
        public List<double> AssemblingEndTimes { get; set; }
        public List<double> AssemblingDurations { get; set; }
        public double AssemblingStartTime { get; set; }
        public double AssemblingEndTime { get; set; }
        public double AssemblingDuration { get; set; }
        public double Cost { get; set; }
        public double GroupsCount { get; set; }
        public List<double> ProductsCountInBatch { get; set; }
    }
}
