﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchedulingCore.Entities
{
    public class DetailsBatch
    {
        public int CountOfDetails { get; set; }
        public Detail Detail { get; set; }

        public List<double> StartTimes { get; set; }
        public List<double> EndTimes { get; set; }
        public List<double> MachiningDurations { get; set; }
        public List<double> DelaysAfterLastTool { get; set; }
    }
}
