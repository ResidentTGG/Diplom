﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchedulingCore.Entities
{
    public class ProductOrder
    {
        public int ProductId { get; set; }
        public int ProductsCount { get; set; }
        public double AllowMachiningCost { get; set; }
    }
}
