﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchedulingCore.Entities;

namespace SchedulingCore.DecisiveRules
{
    public static class Lukr
    {
        public static void SortDetailsBatches(List<ProductBatch> productBatches)
        {
            foreach (var productBatch in productBatches)
            {
                foreach (var detailsBatchGroup in productBatch.DetailsBatchesGroups)
                    detailsBatchGroup.DetailsBatches = detailsBatchGroup.DetailsBatches.OrderBy(r => SumDurations(r)).ToList();
            }
        }

        public static double SumDurations(DetailsBatch detailsBatch)
        {
            double sum = 0;
            foreach (var duration in detailsBatch.MachiningDurations)
                sum += duration;
            return sum;
        }
    }
}
