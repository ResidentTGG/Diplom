﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchedulingCore.Entities;

namespace SchedulingCore.DecisiveRules
{
    public static class MySort
    {
        public static void SortDetailsBatches(List<ProductBatch> productBatches)
        {
            foreach (var productBatch in productBatches)
            {
                foreach (var detailsBatchGroup in productBatch.DetailsBatchesGroups)
                {
                    for (int i = 0; i < detailsBatchGroup.DetailsBatches.Count - 1; i++)
                    {
                        var maxIndex = FindMaxCoincidence(detailsBatchGroup.DetailsBatches, i);
                        SwapBatches(detailsBatchGroup.DetailsBatches, i + 1, maxIndex);
                    }
                }
            }
        }

        private static int FindMaxCoincidence(List<DetailsBatch> detailsBatches, int startIndex)
        {
            int maxCount = 0;
            int index = startIndex;

            for (int j = startIndex + 1; j < detailsBatches.Count; j++)
            {
                if (FindCountCoincidence(detailsBatches[startIndex], detailsBatches[j]) > maxCount)
                {
                    maxCount = FindCountCoincidence(detailsBatches[startIndex], detailsBatches[j]);
                    index = j;
                }
            }
            return index;
        }

        private static int FindCountCoincidence(DetailsBatch detBatchA, DetailsBatch detBatchB)
        {
            int count = 0;
            for (int i = 0; i < detBatchA.Detail.MachiningOrder.Count; i++)
            {
                if (detBatchA.Detail.MachiningOrder[i] == detBatchB.Detail.MachiningOrder[i])
                    count++;
            }
            return count;
        }

        private static void SwapBatches(List<DetailsBatch> detailsBatches, int a, int b)
        {
            var buf = detailsBatches[a];
            detailsBatches[a] = detailsBatches[b];
            detailsBatches[b] = buf;
        }

        public static double SumDurations(DetailsBatch detailsBatch)
        {
            double sum = 0;
            foreach (var duration in detailsBatch.MachiningDurations)
                sum += duration;
            return sum;
        }


    }
}
