﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchedulingCore.Entities;

namespace SchedulingCore.DecisiveRules
{
    public static class Spt
    {
        public static void SortDetailsBatches(List<ProductBatch> productBatches)
        {
            foreach (var productBatch in productBatches)
            {
                foreach (var detailsBatchGroup in productBatch.DetailsBatchesGroups)
                    detailsBatchGroup.DetailsBatches = detailsBatchGroup.DetailsBatches.OrderBy(r => FindTimeFirstTool(r)).ToList();
            }
        }

        public static double FindTimeFirstTool(DetailsBatch detailsBatch)
        {
            var index = detailsBatch.Detail.MachiningOrder.IndexOf(1);
            return detailsBatch.MachiningDurations[index];
        }
    }
}
