﻿using Newtonsoft.Json;
using SchedulingCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SchedulingCore.DecisiveRules
{
    public static class AllRules
    {
        public static void SortDetailsBatches(List<ProductBatch> productBatches)
        {
            var json = JsonConvert.SerializeObject(productBatches);

            var fakeProductBatches = JsonConvert.DeserializeObject<List<ProductBatch>>(json);
            Bonding.BondDetailsBatches(fakeProductBatches);
            TimeCounter.CountProductMachiningTime(fakeProductBatches);
            double time = CountSumProductsMachiningTime(fakeProductBatches);
         
            int rule = 0;

            fakeProductBatches = JsonConvert.DeserializeObject<List<ProductBatch>>(json);
            Lukr.SortDetailsBatches(fakeProductBatches);
            Bonding.BondDetailsBatches(fakeProductBatches);
            TimeCounter.CountProductMachiningTime(fakeProductBatches);
            var sumTime = CountSumProductsMachiningTime(fakeProductBatches);
            if (sumTime < time)
            {
                rule = 1;
                time = sumTime;
            }

            fakeProductBatches = JsonConvert.DeserializeObject<List<ProductBatch>>(json);
            InverseLukr.SortDetailsBatches(fakeProductBatches);
            Bonding.BondDetailsBatches(fakeProductBatches);
            TimeCounter.CountProductMachiningTime(fakeProductBatches);
            sumTime = CountSumProductsMachiningTime(fakeProductBatches);
            if (sumTime < time)
            {
                rule = 2;
                time = sumTime;
            }

            fakeProductBatches = JsonConvert.DeserializeObject<List<ProductBatch>>(json);
            Spt.SortDetailsBatches(fakeProductBatches);
            Bonding.BondDetailsBatches(fakeProductBatches);
            TimeCounter.CountProductMachiningTime(fakeProductBatches);
            sumTime = CountSumProductsMachiningTime(fakeProductBatches);
            if (sumTime < time)
            {
                rule = 3;
                time = sumTime;
            }

            fakeProductBatches = JsonConvert.DeserializeObject<List<ProductBatch>>(json);
            MySort.SortDetailsBatches(fakeProductBatches);
            Bonding.BondDetailsBatches(fakeProductBatches);
            TimeCounter.CountProductMachiningTime(fakeProductBatches);
            sumTime = CountSumProductsMachiningTime(fakeProductBatches);
            if (sumTime < time)
            {
                rule = 4;
                time = sumTime;
            }

            fakeProductBatches = JsonConvert.DeserializeObject<List<ProductBatch>>(json);
            Lpt.SortDetailsBatches(fakeProductBatches);
            Bonding.BondDetailsBatches(fakeProductBatches);
            TimeCounter.CountProductMachiningTime(fakeProductBatches);
            sumTime = CountSumProductsMachiningTime(fakeProductBatches);
            if (sumTime < time)
            {
                rule = 5;
                time = sumTime;
            }

            switch (rule)
            {
                case 1:
                    Lukr.SortDetailsBatches(productBatches);
                    break;
                case 2:
                    InverseLukr.SortDetailsBatches(productBatches);
                    break;
                case 3:
                    Spt.SortDetailsBatches(productBatches);
                    break;
                case 4:
                    MySort.SortDetailsBatches(productBatches);
                    break;
                case 5:
                    Lpt.SortDetailsBatches(productBatches);
                    break;
                default:                                                           
                    break;
            }
            Bonding.BondDetailsBatches(productBatches);
            TimeCounter.CountProductMachiningTime(productBatches);
        }

        public static double CountSumProductsMachiningTime(List<ProductBatch> productBatches)
        {
            double time = 0;
            foreach (var pBatch in productBatches)
            {
                time += pBatch.Product.MachiningDuration;
            }
            return time;
        }
    }
}
