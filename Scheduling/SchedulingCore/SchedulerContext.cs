﻿using SchedulingCore.DecisiveRules;
using SchedulingCore.Entities;
using System;
using System.Collections.Generic;


namespace SchedulingCore
{
    public class SchedulerContext
    {
        public List<ProductBatch> BuildSchedule(List<ProductOrder> productsOrders, int rule)
        {

           
       
            //Считывание данных о деталях
            List<Detail> details = WorkingWithFiles.ReadDetailsFile();
            //Считывание данных об изделиях
            List<FileProduct> products = WorkingWithFiles.ReadProductsFile();

            var watch = System.Diagnostics.Stopwatch.StartNew();

            //Формирование партий изделий из входных данных, 
            //также подсчет времен сборки и продолжительности обработок партий деталей
            var productBatches = Formation.FormProductsBatches(productsOrders, products, details);

            //Сортировка партий деталей внутри 1 группы изделия по решающим правилам
            SortDecisiveRules.SortDetailsBatches(productBatches, rule);

            //Склейка партий деталей внутри 1 изделия 
            if (rule != 0)
                Bonding.BondDetailsBatches(productBatches);

            TimeCounter.CountProductMachiningTime(productBatches);

            //Расчет времен обработки для партий изделий
            TimeCounter.CountProductBatchesTimes(productBatches);

            //Сортировка партий изделий 
            switch (rule)
            {
                case -1:
                    break;
                default:
                    Sorting.SortProductBatches(productBatches);
                    break;
            }

            //Склейка партий изделий 
            Bonding.BondProductBatches(productBatches);

            //Подсчет затрат на пр-во каждой партии изделий
            TimeCounter.CountProductsBatchesCost(productBatches);

            //Небольшая сортировка для отправки клиенту
            Sorting.SortToolOrder(productBatches);


            watch.Stop();
            var elapsedMs = watch.ElapsedMilliseconds;
            Console.WriteLine(elapsedMs +"\n");

            return productBatches;
        }
    }
}
