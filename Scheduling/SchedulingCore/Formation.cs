﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchedulingCore.Entities;

namespace SchedulingCore
{
    public static class Formation
    {
        public static List<ProductBatch> FormProductsBatches(List<ProductOrder> productsOrders, List<FileProduct> fileProducts, List<Detail> details)
        {
            var products = Formation.FormProducts(productsOrders, fileProducts, details);

            var productBatches = new List<ProductBatch>();

            foreach (var productOrder in productsOrders)
            {

                var productBatch = new ProductBatch();
                productBatch.CountOfProducts = productOrder.ProductsCount;

                var product = products.FirstOrDefault(p => p.Id == productOrder.ProductId);
                productBatch.Product = product;

                //Подсчет кол-ва изделий в каждой группе, если в заказе не заданы издержки,то группа одна
                CountSizeOfBatches(productBatch, productOrder);

                var detailsBatchesGroups = new List<DetailsBatchGroup>();
                if (productBatch.GroupsCount == 0)
                {
                    var detailsBatchGroup = new DetailsBatchGroup();
                    var detailsBatches = new List<DetailsBatch>();

                    for (int i = 0; i < product.Details.Count; i++)
                    {
                        var detailsBatch = new DetailsBatch();
                        detailsBatch.Detail = product.Details[i];
                        detailsBatch.CountOfDetails = product.DetailsCount[i];

                        //Считаем продолжительности обработки для партий деталей
                        //Начальные и конечные времена считаются относительно 0
                        TimeCounter.CountMachiningTimes(detailsBatch);

                        detailsBatches.Add(detailsBatch);
                    }
                    detailsBatchGroup.DetailsBatches = detailsBatches;
                    detailsBatchesGroups.Add(detailsBatchGroup);
                }
                else
                {
                    var detailsBatchGroup = new DetailsBatchGroup();
                    var detailsBatches = new List<DetailsBatch>();
                    for (int i = 0; i < product.Details.Count; i++)
                    {
                        var detailsBatch = new DetailsBatch();
                        detailsBatch.Detail = product.Details[i];
                        detailsBatch.CountOfDetails = product.DetailsCount[i] * (int)productBatch.ProductsCountInBatch[0];

                        //Считаем продолжительности обработки для партий деталей
                        //Начальные и конечные времена считаются относительно 0
                        TimeCounter.CountMachiningTimes(detailsBatch);

                        detailsBatches.Add(detailsBatch);
                    }
                    detailsBatchGroup.DetailsBatches = detailsBatches;
                    detailsBatchesGroups.Add(detailsBatchGroup);

                    var detailsSpecialBatchGroup = new DetailsBatchGroup();
                    var detailsSpecialBatches = new List<DetailsBatch>();
                    for (int i = 0; i < product.Details.Count; i++)
                    {
                        var detailsBatch = new DetailsBatch();
                        detailsBatch.Detail = product.Details[i];
                        detailsBatch.CountOfDetails = product.DetailsCount[i] * (int)productBatch.ProductsCountInBatch[1];

                        //Считаем продолжительности обработки для партий деталей
                        //Начальные и конечные времена считаются относительно 0
                        TimeCounter.CountMachiningTimes(detailsBatch);

                        detailsSpecialBatches.Add(detailsBatch);
                    }
                    detailsSpecialBatchGroup.DetailsBatches = detailsSpecialBatches;
                    detailsBatchesGroups.Add(detailsSpecialBatchGroup);
                }

                productBatch.DetailsBatchesGroups = detailsBatchesGroups;

                productBatches.Add(productBatch);
            }
            return productBatches;
        }

        private static void CountSizeOfBatches(ProductBatch productBatch, ProductOrder productOrder)
        {
            var productsCountInBatch = new List<double>();
            double multi = productOrder.AllowMachiningCost / productBatch.Product.Cost;

            if (multi == 0 )
            {
                productBatch.GroupsCount = 0;
                productsCountInBatch.Add(0);
                productsCountInBatch.Add(0);
            }


            else
            {
                if (multi > 10)
                {
                    productBatch.GroupsCount = 10;
                    if (productBatch.GroupsCount > productBatch.CountOfProducts)
                    {
                        productBatch.GroupsCount = 0;
                        productsCountInBatch.Add(0);
                        productsCountInBatch.Add(0);
                    }
                    else
                    {
                        productsCountInBatch.Add(Math.Floor(productBatch.CountOfProducts / productBatch.GroupsCount));
                        productsCountInBatch.Add(productBatch.CountOfProducts - productsCountInBatch[0] * (productBatch.GroupsCount - 1));
                    }
                }
                else
                {
                    productBatch.GroupsCount = Math.Ceiling(multi);

                    productsCountInBatch.Add(Math.Floor(productBatch.CountOfProducts / productBatch.GroupsCount));
                    productsCountInBatch.Add(productBatch.CountOfProducts - productsCountInBatch[0] * (productBatch.GroupsCount - 1));
                }

            }
            productBatch.ProductsCountInBatch = productsCountInBatch;
        }



        //Формируем массив изделий, после этого метода только свойство MachiningDuration пустое
        private static List<Product> FormProducts(List<ProductOrder> productsOrders, List<FileProduct> fileProducts, List<Detail> details)
        {
            var products = new List<Product>();

            foreach (var productOrder in productsOrders)
            {
                var product = new Product();
                var productDetails = new List<Detail>();

                product.Id = productOrder.ProductId;
                var fileProduct = fileProducts.FirstOrDefault(p => p.Id == product.Id);
                product.Name = fileProduct.Name;
                product.DetailsCount = fileProduct.DetailsCount;
                foreach (var detailId in fileProduct.DetailIds)
                    productDetails.Add(details.FirstOrDefault(d => d.Id == detailId));

                product.Details = productDetails;
                products.Add(product);
            }
            //Заполняем поле AssemblingTime у изделий(Product)
            TimeCounter.CountAssemblingTime(products);
            CountCosts(products);
            return products;
        }

        private static void CountCosts(List<Product> products)
        {
            foreach (var product in products)
            {
                double sum = 0;
                for (int i = 0; i < product.Details.Count; i++)
                {
                    sum += product.Details[i].ReadjustmentCost;
                }
                product.Cost = sum;
            }
        }
    }
}
