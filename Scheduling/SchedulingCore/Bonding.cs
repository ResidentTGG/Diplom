﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchedulingCore.Entities;

namespace SchedulingCore
{
    public static class Bonding
    {
        //Расчет времен обработки партий деталей внутри 1 изделия
        internal static void BondDetailsBatches(List<ProductBatch> productBatches)
        {
            foreach (var productBatch in productBatches)
                foreach (var detailsBatchGroup in productBatch.DetailsBatchesGroups)
                {
                    for (int i = 1; i < detailsBatchGroup.DetailsBatches.Count; i++)
                    {
                        for (int j = 0; j < detailsBatchGroup.DetailsBatches[i].MachiningDurations.Count; j++)
                        {
                            if (detailsBatchGroup.DetailsBatches[i].MachiningDurations[j] == 0)
                                continue;
                            //Если продолжительность обработки предыдущей партии деталей на этом станке равна 0, берем партию до этой
                            int k = i;
                            while (k != 0 && FindDuration(detailsBatchGroup.DetailsBatches[k - 1], detailsBatchGroup.DetailsBatches[k - 1].Detail.MachiningOrder[j]) == 0)
                                k--;
                            if (k == 0)
                                k = 1;

                            var startTime = detailsBatchGroup.DetailsBatches[i].StartTimes[j];
                            var endTimePreviousBatch = FindEndTime(detailsBatchGroup.DetailsBatches[k - 1], detailsBatchGroup.DetailsBatches[k].Detail.MachiningOrder[j]);
                            var diff = startTime - endTimePreviousBatch;
                            if (j == 0)
                                MoveTimes(detailsBatchGroup.DetailsBatches[i], diff,0);
                            else if (diff <= 0)
                                MoveTimes(detailsBatchGroup.DetailsBatches[i], diff,j);
                        }
                    }
                }
            
        }

        internal static void BondProductBatches(List<ProductBatch> productBatches)
        {
            for (int i = 1; i < productBatches.Count; i++)
            {
                var diff = productBatches[i].AssemblingStartTime - productBatches[i].MachiningStartTime;
                productBatches[i].MachiningStartTime = productBatches[i - 1].MachiningEndTime;
                productBatches[i].MachiningEndTime = productBatches[i].MachiningStartTime + productBatches[i].MachiningDuration;
                
                productBatches[i].AssemblingStartTime = productBatches[i].MachiningStartTime+ diff;
               
                var diff2 = productBatches[i].AssemblingStartTime - productBatches[i - 1].AssemblingEndTime;             
                if (diff2 >= 0)
                {               
                    productBatches[i].AssemblingEndTime = productBatches[i].AssemblingStartTime + productBatches[i].AssemblingDuration;
                }
                else
                {
                    productBatches[i].MachiningStartTime -= diff2;
                    productBatches[i].MachiningEndTime -= diff2;
                    productBatches[i].AssemblingStartTime -= diff2;                
                    productBatches[i].AssemblingEndTime = productBatches[i].AssemblingStartTime + productBatches[i].AssemblingDuration;
                }

            }
            MoveAssemblingTimes(productBatches);
        }

        private static void MoveAssemblingTimes(List<ProductBatch> productBatches)
        {
            for(int i=0;i<productBatches.Count;i++)
            {
                var diff = productBatches[i].AssemblingStartTime - productBatches[i].AssemblingStartTimes[0];
                for(int j=0;j< productBatches[i].AssemblingStartTimes.Count;j++)
                {
                    productBatches[i].AssemblingStartTimes[j] += diff;
                        productBatches[i].AssemblingEndTimes[j] += diff;
                    
                }
            }
        }




        //Сдвигает все начальные и конечные времена в партии на diff 
        private static void MoveTimes(DetailsBatch detailsBatch, double diff,int startIndex)
        {
            for (int i = startIndex; i < detailsBatch.StartTimes.Count; i++)
                detailsBatch.StartTimes[i] -= diff;
            for (int i = startIndex; i < detailsBatch.EndTimes.Count; i++)
                detailsBatch.EndTimes[i] -= diff;
        }

        //Ищет конечное время обработки партии деталей на станке
        private static double FindEndTime(DetailsBatch detailsBatch, int toolType)
        {
            var stageIndex = detailsBatch.Detail.MachiningOrder.IndexOf(toolType);
            double endTime = detailsBatch.EndTimes[stageIndex];
            return endTime;
        }

        //Ищет продолжительность обработки партии деталей на станке
        private static double FindDuration(DetailsBatch detailsBatch, int toolType)
        {
            var stageIndex = detailsBatch.Detail.MachiningOrder.IndexOf(toolType);
            double duration = detailsBatch.MachiningDurations[stageIndex];
            return duration;
        }



    }
}
