﻿using SchedulingCore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SchedulingCore
{
    public static class RandomBase
    {


        public static void Fill(List<FileProduct> products)
        {
   
            for(int i=0;i<products.Count;i++)
            {
                products[i].Id = i + 1;
            }

            //Задать кол-во деталей в каждом изделии
            foreach (var product in products)
            {
                int countOfDetails = 102;
                var detailIds = new List<int>();
                var detailsCounts = new List<int>();
                for (int i = 0; i < countOfDetails; i++)
                {
                    detailIds.Add(i + 1);
                    detailsCounts.Add(1);
                }
                product.DetailIds = detailIds;
                product.DetailsCount = detailsCounts;
            }

            var rand = new Random();
            for (int i = 0; i < products.Count; i++)
                for (int j = 0; j < products[i].DetailIds.Count; j++)
                {
                    products[i].DetailsCount[j] = rand.Next(5, 10);
                }
        }

        public static void Fill(List<Detail> details)
        {

            for (int i = 0; i < details.Count; i++)
            {
                details[i].Id = i + 1;
            }

            //Задать кол-во станков для каждой детали
            for (int i = 0; i < details.Count; i++)
            {
                int countTools = 10;
                var machOrder = new List<int>();
                var machTimes = new List<double>();
                var readTimes = new List<double>();

                for (int j = 1; j <= countTools; j++)
                {
                    machOrder.Add(j);
                    machTimes.Add(j);
                    readTimes.Add(j);
                }
                details[i].MachiningOrder = machOrder;
                details[i].MachiningTimes = machTimes;
                details[i].ReadjustmentTimes = readTimes;
            }

            var rand = new Random();
            for (int i = 0; i < details.Count; i++)
            {
                details[i].ReadjustmentCost = rand.Next(20, 50);

                for (int j = 0; j < details[i].MachiningOrder.Count; j++)
                {
                    details[i].MachiningOrder.Shuffle(rand);
                    if (details[i].MachiningTimes[j] != 0)
                        details[i].MachiningTimes[j] = rand.Next(10, 25);
                    if (details[i].ReadjustmentTimes[j] != 0)
                        details[i].ReadjustmentTimes[j] = rand.Next(10, 15);
                    
                }
                for (int j = 0; j < details[i].AssemblingTimes.Count; j++)
                {
                    details[i].AssemblingTimes[j] = rand.Next(1, 3);
                }
            }
        }

        private static void Shuffle<T>(this IList<T> list, Random rand)
        {

            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rand.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

    }
}
