﻿using Microsoft.AspNetCore.Hosting;
using Nancy;
using Nancy.Conventions;

namespace HttpServer
{
    public class AppNancyBootstrapper : DefaultNancyBootstrapper
    {
        public AppNancyBootstrapper(IHostingEnvironment environment)
        {
            RootPathProvider = new AppRootPathProvider(environment);
        }
        protected override void ConfigureConventions(NancyConventions conventions)
        {
            base.ConfigureConventions(conventions);
            conventions.StaticContentsConventions.Add(StaticContentConventionBuilder.AddDirectory("/", "Content"));
        }
        protected override IRootPathProvider RootPathProvider { get; }
    }
}