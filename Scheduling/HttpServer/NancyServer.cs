﻿using Nancy;
using System.Collections.Generic;
using Nancy.ModelBinding;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Nancy.Owin;
using SchedulingCore;
using SchedulingCore.Entities;

namespace HttpServer
{
    public class NancyServer : NancyModule
    {
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseOwin(x => x.UseNancy(options => options.Bootstrapper = new AppNancyBootstrapper(env)));
        }

        public NancyServer()
        {
            Get("/", args => Index(args));
            Get("/GetFileProducts", args => GetFileProducts(args));
            Get("/GetDetails", args => GetDetails(args));

            Post("/BuildSchedule/{rule}", args => BuildSchedule(args));
        }

        private dynamic Index(dynamic parameters)
        {
            //При каждом запуске генерировать новую базу
            WorkingWithFiles.CreateData();
  
            return View["Content/index.html"];
        }

        private Response BuildSchedule(dynamic parameters)
        {
            int rule = parameters.rule;
            List<ProductOrder> requestBody = this.Bind<List<ProductOrder>>();

            //var orders = new List<ProductOrder>();
            //for (int i = 1; i <= 10; i++)
            //   orders.Add(new ProductOrder {ProductId=i,ProductsCount=1000,AllowMachiningCost=10000 });

            var context = new SchedulerContext();
            var result = context.BuildSchedule(requestBody,rule);

            return Response.AsJson(result);
        }



        private Response GetFileProducts(dynamic parameters)
        {
            var result = WorkingWithFiles.ReadProductsFile();
            return Response.AsJson(result);
        }

        private Response GetDetails(dynamic parameters)
        {
            var result = WorkingWithFiles.ReadDetailsFile();
            return Response.AsJson(result);
        }
    }
}
