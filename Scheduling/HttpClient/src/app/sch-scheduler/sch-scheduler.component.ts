import { Component, OnInit } from '@angular/core';
import { Product } from '../entities/product'
import { FileProduct } from '../entities/fileproduct'
import { Detail } from '../entities/detail'
import { DetailsBatch } from '../entities/detailsBatch'
import { DetailsBatchGroup } from '../entities/detailsBatchGroup'
import { ProductBatch } from '../entities/productBatch'
import { ProductOrder } from '../entities/productOrder'
import { ApiService } from '../services/api.service';

@Component({
  selector: 'sch-scheduler',
  templateUrl: './sch-scheduler.component.html',
  styleUrls: ['./sch-scheduler.component.css']
})
export class SchSchedulerComponent implements OnInit {

  fileProducts: FileProduct[] = [];
  details: Detail[];
  productBatches: ProductBatch[] = [];
  productBatchesRule: ProductBatch[] = [];
  productBatchesBlock: ProductBatch[] = [];
  productId: number;
  productsCount: number;
  orders: ProductOrder[] = [];
  rule: number = -1;
  allowMachiningCost: number;
  ruleMachiningCost: number;
  blockMachiningCost: number;
  constructor(private apiService: ApiService) { }

  ngOnInit() {
    this.apiService.getProducts().subscribe(fileProducts => this.fileProducts = fileProducts);
    this.apiService.getDetails().subscribe(details => this.details = details);
  }

  buildSchedule() {
    let rule: number = -1;
    let orders = new Array<ProductOrder>();
    this.orders.forEach(element => {
      let order = new ProductOrder();
      order.allowMachiningCost = 0;
      order.productId = element.productId;
      order.productsCount = element.productsCount;
      orders.push(order);
    });
    debugger;
    this.apiService.getResults(orders, rule).subscribe(productBatches => {
      this.productBatches = productBatches;
      document.getElementById("butt-draw").removeAttribute("disabled");
      document.getElementById("butt-draw-batches").removeAttribute("disabled");
    });
  }

  buildScheduleWithRule() {
    if (this.rule != -1) {
      let orders = new Array<ProductOrder>();
      this.orders.forEach(element => {
        let order = new ProductOrder();
        order.allowMachiningCost = 0;
        order.productId = element.productId;
        order.productsCount = element.productsCount;
        orders.push(order);
      });
      this.apiService.getResults(orders, this.rule).subscribe(productBatches => {
        this.productBatchesRule = productBatches;
        document.getElementById("butt-draw-rule").removeAttribute("disabled");
        document.getElementById("rule-butt-draw-batches").removeAttribute("disabled");
        document.getElementById("table-results-rule").removeAttribute("disabled");
      });
    }
  }
  showBatchesResult() {
    document.getElementById("div-batches-results").style.display = "block";
  }
  buildScheduleWithBlock() {
    if (this.rule != -1) {
      this.apiService.getResults(this.orders, this.rule).subscribe(productBatches => {
        this.productBatchesBlock = productBatches;
        document.getElementById("butt-draw-block").removeAttribute("disabled");
        document.getElementById("block-butt-draw-batches").removeAttribute("disabled");
      });
    }
  }

  logRadio(element: HTMLInputElement) {
    this.rule = parseInt(`${element.value}`);
  }

  drawGraph() {
    document.getElementById("all-graph").style.display = "block";
    document.getElementById("scale").style.display = "inline-block";

    for (let i = 0; i < this.productBatches.length; i++) {
      let productId = this.productBatches[i].product.id.toString();

      let machelement = document.getElementById(productId);
      let color = this.getRandomColor();

      let machDur = this.productBatches[i].machiningDuration.toString();
      machelement.style.width = `${machDur}` + "px";
      machelement.style.maxWidth = `${machDur}` + "px";
      machelement.style.minWidth = `${machDur}` + "px";
      machelement.style.backgroundColor = color;
      if (i == 0) {
        let machStart = this.productBatches[i].machiningStartTime.toString();
        machelement.style.marginLeft = (`${machStart}px`);
      }
      else {
        let machStart = this.productBatches[i].machiningStartTime;
        let machEnd = this.productBatches[i - 1].machiningEndTime;
        let margleft = machStart - machEnd;
        let strmargleft = margleft.toString();

        machelement.style.marginLeft = (`${strmargleft}px`);
      }

      let asselement = document.getElementById("ass-" + productId);

      let assDur = this.productBatches[i].assemblingDuration.toString();
      asselement.style.width = `${assDur}` + "px";
      asselement.style.maxWidth = `${assDur}` + "px";
      asselement.style.minWidth = `${assDur}` + "px";
      asselement.style.backgroundColor = color;

      if (i == 0) {
        let assStart = this.productBatches[i].assemblingStartTime.toString();
        asselement.style.marginLeft = (`${assStart}px`);
      }
      else {
        let assStart = this.productBatches[i].assemblingStartTime;
        let assEnd = this.productBatches[i - 1].assemblingEndTime;
        let margleft = assStart - assEnd;
        let strmargleft = margleft.toString()
        asselement.style.marginLeft = (`${strmargleft}px`);
      }
    }
  }

  drawGraphRule() {
    document.getElementById("rule-all-graph").style.display = "block";
    document.getElementById("rule-scale").style.display = "inline-block";

    for (let i = 0; i < this.productBatchesRule.length; i++) {
      let productId = this.productBatchesRule[i].product.id.toString();

      let machelement = document.getElementById(`rule-${productId}`);
      let color = this.getRandomColor();

      let machDur = this.productBatchesRule[i].machiningDuration.toString();
      machelement.style.width = `${machDur}` + "px";
      machelement.style.maxWidth = `${machDur}` + "px";
      machelement.style.minWidth = `${machDur}` + "px";
      machelement.style.backgroundColor = color;
      if (i == 0) {
        let machStart = this.productBatchesRule[i].machiningStartTime.toString();
        machelement.style.marginLeft = (`${machStart}px`);
      }
      else {
        let machStart = this.productBatchesRule[i].machiningStartTime;
        let machEnd = this.productBatchesRule[i - 1].machiningEndTime;
        let margleft = machStart - machEnd;
        let strmargleft = margleft.toString();

        machelement.style.marginLeft = (`${strmargleft}px`);
      }

      let asselement = document.getElementById("rule-ass-" + productId);

      let assDur = this.productBatchesRule[i].assemblingDuration.toString();
      asselement.style.width = `${assDur}` + "px";
      asselement.style.maxWidth = `${assDur}` + "px";
      asselement.style.minWidth = `${assDur}` + "px";
      asselement.style.backgroundColor = color;

      if (i == 0) {
        let assStart = this.productBatchesRule[i].assemblingStartTime.toString();
        asselement.style.marginLeft = (`${assStart}px`);
      }
      else {
        let assStart = this.productBatchesRule[i].assemblingStartTime;
        let assEnd = this.productBatchesRule[i - 1].assemblingEndTime;
        let margleft = assStart - assEnd;
        let strmargleft = margleft.toString()
        asselement.style.marginLeft = (`${strmargleft}px`);
      }
    }
    this.ruleMachiningCost = this.CountCost(this.productBatchesRule);
  }

  drawGraphBlock() {
    document.getElementById("block-all-graph").style.display = "block";
    document.getElementById("block-scale").style.display = "inline-block";

    for (let i = 0; i < this.productBatchesBlock.length; i++) {
      let productId = this.productBatchesBlock[i].product.id.toString();
      let color = this.getRandomColor();
      for (let j = 0; j < this.productBatchesBlock[i].machiningDurations.length; j++) {

        let machelement = document.getElementById(`block-${productId}-${j}`);

        let machDur = this.productBatchesBlock[i].machiningDurations[j].toString();
        machelement.style.width = `${machDur}` + "px";
        machelement.style.maxWidth = `${machDur}` + "px";
        machelement.style.minWidth = `${machDur}` + "px";
        machelement.style.backgroundColor = color;
        if (i == 0 && j == 0) {
          let machStart = this.productBatchesBlock[i].machiningStartTime.toString();
          machelement.style.marginLeft = (`${machStart}px`);
        }
        else if (i != 0 && j == 0) {
          let machStart = this.productBatchesBlock[i].machiningStartTime;
          let machEnd = this.productBatchesBlock[i - 1].machiningEndTime;
          let margleft = machStart - machEnd;
          let strmargleft = margleft.toString()
          machelement.style.marginLeft = (`${strmargleft}px`);
        }
      }

      for (let j = 0; j < this.productBatchesBlock[i].assemblingStartTimes.length; j++) {
        let asselement = document.getElementById(`block-ass-${productId}-${j}`);
        let assDur = this.productBatchesBlock[i].assemblingDurations[j].toString();
        asselement.style.width = `${assDur}` + "px";
        asselement.style.maxWidth = `${assDur}` + "px";
        asselement.style.minWidth = `${assDur}` + "px";
        asselement.style.backgroundColor = color;

        if (i == 0 && j == 0) {
          let assStart = this.productBatchesBlock[i].assemblingStartTime.toString();
          asselement.style.marginLeft = (`${assStart}px`);
        }
        else if (i != 0 && j == 0) {
          let assStart = this.productBatchesBlock[i].assemblingStartTimes[j];
          let assEnd = this.productBatchesBlock[i - 1].assemblingEndTime;
          let margleft = assStart - assEnd;
          let strmargleft = margleft.toString()
          asselement.style.marginLeft = (`${strmargleft}px`);
        }
        else {
          let assStart = this.productBatchesBlock[i].assemblingStartTimes[j];
          let assEnd = this.productBatchesBlock[i].assemblingEndTimes[j - 1];
          let margleft = assStart - assEnd;
          let strmargleft = margleft.toString()
          asselement.style.marginLeft = (`${strmargleft}px`);
        }
      }

    }
    this.blockMachiningCost = this.CountCost(this.productBatchesBlock);
  }

  drawBatchesGraph() {
    document.getElementById("all-graph-batches").style.display = "block";
    this.productBatches.forEach(element => {
      document.getElementById("scale-" + `${element.product.id}`).style.display = "inline-block";
    });

    for (let i = 0; i < this.productBatches.length; i++) {

      for (let k = 0; k < this.productBatches[i].product.details[0].machiningOrder.length; k++) {

        for (let j = 0; j < this.productBatches[i].detailsBatchesGroups[0].detailsBatches.length; j++) {

          let batchElement = document.getElementById(`${this.productBatches[i].product.id}-` + `${k}-` + `${this.productBatches[i].detailsBatchesGroups[0].detailsBatches[j].detail.id}`);

          let color: string;
          if (k == 0)
          { color = this.getRandomColor(); }
          else {
            let prevbatchElement = document.getElementById(`${this.productBatches[i].product.id}-` + `${k - 1}-` + `${this.productBatches[i].detailsBatchesGroups[0].detailsBatches[j].detail.id}`);
            color = prevbatchElement.style.backgroundColor;
          }

          let machDur = this.productBatches[i].detailsBatchesGroups[0].detailsBatches[j].machiningDurations[k].toString();

          if (machDur == '0') {
            document.getElementById(`numbers-in-blocks-${this.productBatches[i].product.id}-` + `${k}-` + `${this.productBatches[i].detailsBatchesGroups[0].detailsBatches[j].detail.id}`).innerHTML = '';
          }

          batchElement.style.width = `${machDur}` + "px";
          batchElement.style.maxWidth = `${machDur}` + "px";
          batchElement.style.minWidth = `${machDur}` + "px";
          batchElement.style.backgroundColor = color;

          if (j == 0) {
            let machStart = this.productBatches[i].detailsBatchesGroups[0].detailsBatches[j].startTimes[k];
            batchElement.style.marginLeft = (`${machStart}px`);
          }
          else {
            let machStart = this.productBatches[i].detailsBatchesGroups[0].detailsBatches[j].startTimes[k];

            let machEnd = this.productBatches[i].detailsBatchesGroups[0].detailsBatches[j - 1].endTimes[k];

            let margleft = machStart - machEnd;
            let strmargleft = margleft.toString();

            batchElement.style.marginLeft = (`${strmargleft}px`);
          }
        }
      }
    }
  }
  drawBatchesGraphRule() {
    document.getElementById("rule-all-graph-batches").style.display = "block";
    this.productBatchesRule.forEach(element => {
      document.getElementById("rule-scale-" + `${element.product.id}`).style.display = "inline-block";
    });

    for (let i = 0; i < this.productBatchesRule.length; i++) {

      for (let k = 0; k < this.productBatchesRule[i].product.details[0].machiningOrder.length; k++) {

        for (let j = 0; j < this.productBatchesRule[i].detailsBatchesGroups[0].detailsBatches.length; j++) {

          let batchElement = document.getElementById(`rule-${this.productBatchesRule[i].product.id}-` + `${k}-` + `${this.productBatchesRule[i].detailsBatchesGroups[0].detailsBatches[j].detail.id}`);

          let color: string;
          if (k == 0)
          { color = this.getRandomColor(); }
          else {
            let prevbatchElement = document.getElementById(`rule-${this.productBatchesRule[i].product.id}-` + `${k - 1}-` + `${this.productBatchesRule[i].detailsBatchesGroups[0].detailsBatches[j].detail.id}`);
            color = prevbatchElement.style.backgroundColor;
          }

          let machDur = this.productBatchesRule[i].detailsBatchesGroups[0].detailsBatches[j].machiningDurations[k].toString();

          if (machDur == '0') {
            document.getElementById(`rule-numbers-in-blocks-${this.productBatchesRule[i].product.id}-` + `${k}-` + `${this.productBatchesRule[i].detailsBatchesGroups[0].detailsBatches[j].detail.id}`).innerHTML = '';
          }

          batchElement.style.width = `${machDur}` + "px";
          batchElement.style.maxWidth = `${machDur}` + "px";
          batchElement.style.minWidth = `${machDur}` + "px";
          batchElement.style.backgroundColor = color;
          if (j == 0) {
            let machStart = this.productBatchesRule[i].detailsBatchesGroups[0].detailsBatches[j].startTimes[k];
            batchElement.style.marginLeft = (`${machStart}px`);
          }
          else {
            let machStart = this.productBatchesRule[i].detailsBatchesGroups[0].detailsBatches[j].startTimes[k];
            let e = j - 1;
            while (e != 0 && this.productBatchesRule[i].detailsBatchesGroups[0].detailsBatches[e].machiningDurations[k] == 0)
              e--;
            let machEnd = this.productBatchesRule[i].detailsBatchesGroups[0].detailsBatches[e].endTimes[k];
            let margleft = machStart - machEnd;
            let strmargleft = margleft.toString();

            batchElement.style.marginLeft = (`${strmargleft}px`);
          }
        }
      }
    }

  }
  drawBatchesGraphBlock() {
    document.getElementById("block-all-graph-batches").style.display = "block";
    this.productBatchesBlock.forEach(element => {
      document.getElementById("block-scale-" + `${element.product.id}`).style.display = "inline-block";
    });

    for (let i = 0; i < this.productBatchesBlock.length; i++) {

      for (let k = 0; k < this.productBatchesBlock[i].product.details[0].machiningOrder.length; k++) {

        for (let j = 0; j < this.productBatchesBlock[i].detailsBatchesGroups[0].detailsBatches.length; j++) {

          let batchElement = document.getElementById(`block-${this.productBatchesBlock[i].product.id}-` + `${k}-` + `${this.productBatchesBlock[i].detailsBatchesGroups[0].detailsBatches[j].detail.id}`);

          let color: string;
          if (k == 0)
          { color = this.getRandomColor(); }
          else {
            let prevbatchElement = document.getElementById(`block-${this.productBatchesBlock[i].product.id}-` + `${k - 1}-` + `${this.productBatchesBlock[i].detailsBatchesGroups[0].detailsBatches[j].detail.id}`);
            color = prevbatchElement.style.backgroundColor;
          }

          let machDur = this.productBatchesBlock[i].detailsBatchesGroups[0].detailsBatches[j].machiningDurations[k].toString();

          if (machDur == '0') {
            document.getElementById(`block-numbers-in-blocks-${this.productBatchesBlock[i].product.id}-` + `${k}-` + `${this.productBatchesBlock[i].detailsBatchesGroups[0].detailsBatches[j].detail.id}`).innerHTML = '';
          }

          batchElement.style.width = `${machDur}` + "px";
          batchElement.style.maxWidth = `${machDur}` + "px";
          batchElement.style.minWidth = `${machDur}` + "px";
          batchElement.style.backgroundColor = color;
          if (j == 0) {
            let machStart = this.productBatchesBlock[i].detailsBatchesGroups[0].detailsBatches[j].startTimes[k];
            batchElement.style.marginLeft = (`${machStart}px`);
          }
          else {
            let machStart = this.productBatchesBlock[i].detailsBatchesGroups[0].detailsBatches[j].startTimes[k];
            let e = j - 1;
            while (e != 0 && this.productBatchesBlock[i].detailsBatchesGroups[0].detailsBatches[e].machiningDurations[k] == 0)
              e--;
            let machEnd = this.productBatchesBlock[i].detailsBatchesGroups[0].detailsBatches[e].endTimes[k];
            let margleft = machStart - machEnd;
            let strmargleft = margleft.toString();

            batchElement.style.marginLeft = (`${strmargleft}px`);
          }
        }
      }
    }

  }


  addOrder() {
    let productOrder = new ProductOrder();
    productOrder.productId = this.productId;
    productOrder.productsCount = this.productsCount;
    if (isFinite(this.allowMachiningCost) && this.allowMachiningCost > 0)
      productOrder.allowMachiningCost = this.allowMachiningCost;
    else
      productOrder.allowMachiningCost = 0;
    this.orders.push(productOrder);
    document.getElementById("butt-build").removeAttribute("disabled");
    document.getElementById("butt-build-rule").removeAttribute("disabled");
    document.getElementById("butt-build-block").removeAttribute("disabled");
  }

  getRandomColor() {
    let r = Math.floor(Math.random() * (256));
    let g = Math.floor(Math.random() * (256));
    let b = Math.floor(Math.random() * (256));
    let c = '#' + r.toString(16) + g.toString(16) + b.toString(16);
    return c;
  }

  plusScale() {
    for (let i = 0; i < this.productBatches.length; i++) {
      let productId = this.productBatches[i].product.id.toString();

      let machelement = document.getElementById(productId);
      let asselement = document.getElementById("ass-" + productId);

      let newwidth = parseFloat(machelement.style.width) * 1.2;
      machelement.style.width = `${newwidth}` + "px";
      machelement.style.maxWidth = `${newwidth}` + "px";
      machelement.style.minWidth = `${newwidth}` + "px";

      let newmargleft = parseFloat(machelement.style.marginLeft) * 1.2;
      machelement.style.marginLeft = `${newmargleft}` + "px";

      newwidth = parseFloat(asselement.style.width) * 1.2;
      asselement.style.width = `${newwidth}` + "px";
      asselement.style.maxWidth = `${newwidth}` + "px";
      asselement.style.minWidth = `${newwidth}` + "px";

      newmargleft = parseFloat(asselement.style.marginLeft) * 1.2;
      asselement.style.marginLeft = `${newmargleft}` + "px";
    }
  }

  minusScale() {
    for (let i = 0; i < this.productBatches.length; i++) {
      let productId = this.productBatches[i].product.id.toString();

      let machelement = document.getElementById(productId);
      let asselement = document.getElementById("ass-" + productId);

      let newwidth = parseFloat(machelement.style.width) * 0.8;
      machelement.style.width = `${newwidth}` + "px";
      machelement.style.maxWidth = `${newwidth}` + "px";
      machelement.style.minWidth = `${newwidth}` + "px";

      let newmargleft = parseFloat(machelement.style.marginLeft) * 0.8;
      machelement.style.marginLeft = `${newmargleft}` + "px";

      newwidth = parseFloat(asselement.style.width) * 0.8;
      asselement.style.width = `${newwidth}` + "px";
      asselement.style.maxWidth = `${newwidth}` + "px";
      asselement.style.minWidth = `${newwidth}` + "px";

      newmargleft = parseFloat(asselement.style.marginLeft) * 0.8;
      asselement.style.marginLeft = `${newmargleft}` + "px";
    }
  }
  plusScaleRule() {
    for (let i = 0; i < this.productBatchesRule.length; i++) {
      let productId = this.productBatchesRule[i].product.id.toString();

      let machelement = document.getElementById(`rule-${productId}`);
      let asselement = document.getElementById("rule-ass-" + productId);

      let newwidth = parseFloat(machelement.style.width) * 1.2;
      machelement.style.width = `${newwidth}` + "px";
      machelement.style.maxWidth = `${newwidth}` + "px";
      machelement.style.minWidth = `${newwidth}` + "px";

      let newmargleft = parseFloat(machelement.style.marginLeft) * 1.2;
      machelement.style.marginLeft = `${newmargleft}` + "px";

      newwidth = parseFloat(asselement.style.width) * 1.2;
      asselement.style.width = `${newwidth}` + "px";
      asselement.style.maxWidth = `${newwidth}` + "px";
      asselement.style.minWidth = `${newwidth}` + "px";

      newmargleft = parseFloat(asselement.style.marginLeft) * 1.2;
      asselement.style.marginLeft = `${newmargleft}` + "px";
    }
  }

  plusScaleBlock() {
    for (let i = 0; i < this.productBatchesBlock.length; i++) {
      let productId = this.productBatchesBlock[i].product.id.toString();

      for (let j = 0; j < this.productBatchesBlock[i].groupsCount; j++) {
        let machelement = document.getElementById(`block-${productId}-${j}`);


        let newwidth = parseFloat(machelement.style.width) * 1.2;
        machelement.style.width = `${newwidth}` + "px";
        machelement.style.maxWidth = `${newwidth}` + "px";
        machelement.style.minWidth = `${newwidth}` + "px";

        let newmargleft = parseFloat(machelement.style.marginLeft) * 1.2;
        machelement.style.marginLeft = `${newmargleft}` + "px";
      }

      for (let j = 0; j < this.productBatchesBlock[i].groupsCount; j++) {
        let asselement = document.getElementById(`block-ass-${productId}-${j}`);
        let newwidth = parseFloat(asselement.style.width) * 1.2;
        asselement.style.width = `${newwidth}` + "px";
        asselement.style.maxWidth = `${newwidth}` + "px";
        asselement.style.minWidth = `${newwidth}` + "px";

        let newmargleft = parseFloat(asselement.style.marginLeft) * 1.2;
        asselement.style.marginLeft = `${newmargleft}` + "px";
      }
    }
  }

  minusScaleBlock() {
    for (let i = 0; i < this.productBatchesBlock.length; i++) {
      let productId = this.productBatchesBlock[i].product.id.toString();

      for (let j = 0; j < this.productBatchesBlock[i].assemblingStartTimes.length; j++) {
        let machelement = document.getElementById(`block-${productId}-${j}`);


        let newwidth = parseFloat(machelement.style.width) * 0.8;
        machelement.style.width = `${newwidth}` + "px";
        machelement.style.maxWidth = `${newwidth}` + "px";
        machelement.style.minWidth = `${newwidth}` + "px";

        let newmargleft = parseFloat(machelement.style.marginLeft) * 0.8;
        machelement.style.marginLeft = `${newmargleft}` + "px";
      }
      for (let j = 0; j < this.productBatchesBlock[i].assemblingStartTimes.length; j++) {
        let asselement = document.getElementById(`block-ass-${productId}-${j}`);
        let newwidth = parseFloat(asselement.style.width) * 0.8;
        asselement.style.width = `${newwidth}` + "px";
        asselement.style.maxWidth = `${newwidth}` + "px";
        asselement.style.minWidth = `${newwidth}` + "px";

        let newmargleft = parseFloat(asselement.style.marginLeft) * 0.8;
        asselement.style.marginLeft = `${newmargleft}` + "px";
      }
    }
  }


  minusScaleRule() {
    for (let i = 0; i < this.productBatchesRule.length; i++) {
      let productId = this.productBatchesRule[i].product.id.toString();

      let machelement = document.getElementById(`rule-${productId}`);
      let asselement = document.getElementById("rule-ass-" + productId);

      let newwidth = parseFloat(machelement.style.width) * 0.8;
      machelement.style.width = `${newwidth}` + "px";
      machelement.style.maxWidth = `${newwidth}` + "px";
      machelement.style.minWidth = `${newwidth}` + "px";

      let newmargleft = parseFloat(machelement.style.marginLeft) * 0.8;
      machelement.style.marginLeft = `${newmargleft}` + "px";

      newwidth = parseFloat(asselement.style.width) * 0.8;
      asselement.style.width = `${newwidth}` + "px";
      asselement.style.maxWidth = `${newwidth}` + "px";
      asselement.style.minWidth = `${newwidth}` + "px";

      newmargleft = parseFloat(asselement.style.marginLeft) * 0.8;
      asselement.style.marginLeft = `${newmargleft}` + "px";
    }
  }

  plusBatchScale(id) {
    let prBatch = this.productBatches.find(pr => pr.product.id == id);
    for (let i = 0; i < prBatch.product.details[0].machiningOrder.length; i++) {
      for (let j = 0; j < prBatch.detailsBatchesGroups[0].detailsBatches.length; j++) {
        let batchElement = document.getElementById(`${id}-` + `${i}-` + `${prBatch.detailsBatchesGroups[0].detailsBatches[j].detail.id}`);

        let newwidth = parseFloat(batchElement.style.width) * 1.2;
        batchElement.style.width = `${newwidth}` + "px";
        batchElement.style.maxWidth = `${newwidth}` + "px";
        batchElement.style.minWidth = `${newwidth}` + "px";

        let newmargleft = parseFloat(batchElement.style.marginLeft) * 1.2;
        batchElement.style.marginLeft = `${newmargleft}` + "px";
      }
    }
  }
  plusBatchScaleRule(id) {
    let prBatch = this.productBatchesRule.find(pr => pr.product.id == id);
    for (let i = 0; i < prBatch.product.details[0].machiningOrder.length; i++) {
      for (let j = 0; j < prBatch.detailsBatchesGroups[0].detailsBatches.length; j++) {
        let batchElement = document.getElementById(`rule-${id}-` + `${i}-` + `${prBatch.detailsBatchesGroups[0].detailsBatches[j].detail.id}`);

        let newwidth = parseFloat(batchElement.style.width) * 1.2;
        batchElement.style.width = `${newwidth}` + "px";
        batchElement.style.maxWidth = `${newwidth}` + "px";
        batchElement.style.minWidth = `${newwidth}` + "px";

        let newmargleft = parseFloat(batchElement.style.marginLeft) * 1.2;
        batchElement.style.marginLeft = `${newmargleft}` + "px";
      }
    }
  }
  plusBatchScaleBlock(id) {
    let prBatch = this.productBatchesBlock.find(pr => pr.product.id == id);
    for (let i = 0; i < prBatch.product.details[0].machiningOrder.length; i++) {
      for (let j = 0; j < prBatch.detailsBatchesGroups[0].detailsBatches.length; j++) {
        let batchElement = document.getElementById(`block-${id}-` + `${i}-` + `${prBatch.detailsBatchesGroups[0].detailsBatches[j].detail.id}`);

        let newwidth = parseFloat(batchElement.style.width) * 1.2;
        batchElement.style.width = `${newwidth}` + "px";
        batchElement.style.maxWidth = `${newwidth}` + "px";
        batchElement.style.minWidth = `${newwidth}` + "px";

        let newmargleft = parseFloat(batchElement.style.marginLeft) * 1.2;
        batchElement.style.marginLeft = `${newmargleft}` + "px";
      }
    }
  }

  minusBatchScale(id) {
    let prBatch = this.productBatches.find(pr => pr.product.id == id);
    for (let i = 0; i < prBatch.product.details[0].machiningOrder.length; i++) {
      for (let j = 0; j < prBatch.detailsBatchesGroups[0].detailsBatches.length; j++) {
        let batchElement = document.getElementById(`${id}-` + `${i}-` + `${prBatch.detailsBatchesGroups[0].detailsBatches[j].detail.id}`);

        let newwidth = parseFloat(batchElement.style.width) * 0.8;
        batchElement.style.width = `${newwidth}` + "px";
        batchElement.style.maxWidth = `${newwidth}` + "px";
        batchElement.style.minWidth = `${newwidth}` + "px";

        let newmargleft = parseFloat(batchElement.style.marginLeft) * 0.8;
        batchElement.style.marginLeft = `${newmargleft}` + "px";
      }
    }
  }
  minusBatchScaleRule(id) {
    let prBatch = this.productBatchesRule.find(pr => pr.product.id == id);
    for (let i = 0; i < prBatch.product.details[0].machiningOrder.length; i++) {
      for (let j = 0; j < prBatch.detailsBatchesGroups[0].detailsBatches.length; j++) {
        let batchElement = document.getElementById(`rule-${id}-` + `${i}-` + `${prBatch.detailsBatchesGroups[0].detailsBatches[j].detail.id}`);

        let newwidth = parseFloat(batchElement.style.width) * 0.8;
        batchElement.style.width = `${newwidth}` + "px";
        batchElement.style.maxWidth = `${newwidth}` + "px";
        batchElement.style.minWidth = `${newwidth}` + "px";

        let newmargleft = parseFloat(batchElement.style.marginLeft) * 0.8;
        batchElement.style.marginLeft = `${newmargleft}` + "px";
      }
    }
  }
  minusBatchScaleBlock(id) {
    let prBatch = this.productBatchesBlock.find(pr => pr.product.id == id);
    for (let i = 0; i < prBatch.product.details[0].machiningOrder.length; i++) {
      for (let j = 0; j < prBatch.detailsBatchesGroups[0].detailsBatches.length; j++) {
        let batchElement = document.getElementById(`block-${id}-` + `${i}-` + `${prBatch.detailsBatchesGroups[0].detailsBatches[j].detail.id}`);

        let newwidth = parseFloat(batchElement.style.width) * 0.8;
        batchElement.style.width = `${newwidth}` + "px";
        batchElement.style.maxWidth = `${newwidth}` + "px";
        batchElement.style.minWidth = `${newwidth}` + "px";

        let newmargleft = parseFloat(batchElement.style.marginLeft) * 0.8;
        batchElement.style.marginLeft = `${newmargleft}` + "px";
      }
    }
  }

  CountCost(productBatches: ProductBatch[]): number {
    let sum: number = 0;
    productBatches.forEach(element => {
      sum += element.cost;
    });
    return sum;
  }
}
