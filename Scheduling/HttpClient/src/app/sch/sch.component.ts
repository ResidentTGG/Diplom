import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './sch.component.html',
  styleUrls: ['./sch.component.css'],
})

export class AppComponent {
  title = 'Построение расписания';
}
