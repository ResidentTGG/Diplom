import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Response, Headers } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { ProductBatch } from '../entities/productBatch';
import { ProductOrder } from '../entities/ProductOrder';
import { FileProduct } from '../entities/fileProduct';
import { Detail } from '../entities/detail';
import { Product } from '../entities/product';

@Injectable()
export class ApiService {
    constructor(private http: Http) { }

    getResults(order: ProductOrder[],rule:number): Observable<ProductBatch[]> {

        const body = JSON.stringify(order);
        let headers = new Headers({ 'Content-Type': 'application/json;charset=utf-8' });

        return this.http.post(`/BuildSchedule/${rule}`, body, { headers: headers })
            .map((resp: Response) => {
                return resp.json() as ProductBatch[]
            })
            .catch((error: any) => { return Observable.throw(error); });
    }


    getProducts(): Observable<FileProduct[]> {
        return this.http.get(`GetFileProducts`)
            .map(response => response.json() as FileProduct[]);
    }

    getDetails(): Observable<Detail[]> {
        return this.http.get(`GetDetails`)
            .map(response => response.json() as Detail[]);
    }
}