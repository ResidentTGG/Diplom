export class FileProduct {
    id: number;
    name: string;
    detailsCount: number[];
    detailIds: number[];
}
