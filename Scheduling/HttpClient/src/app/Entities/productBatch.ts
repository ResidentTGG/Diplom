import { Product } from './product'
import { DetailsBatchGroup } from './detailsBatchGroup'

export class ProductBatch {
    product: Product;
    countOfProducts: number;
    detailsBatchesGroups: DetailsBatchGroup[];
    machiningStartTime: number;
    machiningEndTime: number;
    machiningDuration: number;
    assemblingStartTimes: number[];
    assemblingEndTimes: number[];
    assemblingDurations: number[];
    machiningDurations: number[];
    assemblingStartTime: number;
    assemblingEndTime: number;
    assemblingDuration: number;
    cost: number;
    groupsCount: number;
    productsCountInBatch: number[];
}