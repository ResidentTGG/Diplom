import { Detail } from './detail'

export class Product {
    id: number;
    name: string;
    details: Detail[];
    detailsCount: number[];
    assemblingDuration: number;
    machiningDuration: number;
}
