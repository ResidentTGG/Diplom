export class Detail {
    id: number;
    name: string;
    machiningOrder: number[];
    machiningTimes: number[];
    readjustmentTimes: number[];
    assemblingTimes: number[];
}
