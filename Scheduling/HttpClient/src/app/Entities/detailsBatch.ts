import { Detail } from './detail'

export class DetailsBatch {
    countOfDetails: number;
    detail: Detail;
    startTimes: number[];
    endTimes: number[];
    machiningDurations: number[];
    delaysAfterLastTool: number[];
}
