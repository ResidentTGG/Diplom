import { HttpClientPage } from './app.po';

describe('http-client App', function() {
  let page: HttpClientPage;

  beforeEach(() => {
    page = new HttpClientPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
