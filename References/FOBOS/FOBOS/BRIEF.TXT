      
          THE PROGRAM PACKAGE FOR JUST-IN-TIME CONTROL
             SHORT-TERM  PLANNING  AND  SCHEDULING 
                  IN FLEXIBLE MANUFACTURING

            "FOBOS" (C) 1990,1991  Frolov, Yakovlev
                         Moscow (USSR)                      

   Decision support system in Computer integrated manufacturing
(CIM) caters for short-term  production planning and scheduling 
in main technological components of automated enterprise. These
components of CIM are supplied by individual functional support 
systems. In CIM manufacturing planning, control and  management 
must  exploit "Just in time" (JIT) principle - that is when the
workpieces are fabricated,processed and assembled in the needed
quantity and within a given time-limit. One of the key benefits
of JIT is the ability to manufacture economically in very small
batches or lots.
  The program package "FOBOS" has a powerful user interface and
permits:
      - Short-term Planning within month or decade time-unit, 
      - Machine Loading Problem solution, 
      - Shift Scheduling for workpieces,
      - Real-time Dispatching of material flows,
      - JIT Control in flexible manufacturing.

Short-term planning, scheduling and JIT control problems in FMS
are multicriterial.  These criteria  are determined both  while
choosing priority rules for lots of workpieces:
     1.1 First-in-First-out             (FIFO)
     1.2 Assembly Delay Indicator       (ADI)
     1.3 Longest Queue                  (LNQ)
     1.4 Earliest Due Date              (EDD)
     1.5 Longest Processing Time        (LPT)
     1.6 Most of Remaining Operations

and at the stage of machine-loading:
     2.1 Max Machine Utilization Index
     2.2 Min Number of Machines involved
     2.3 Even average Machine Utilization
     2.4 Min Number of Set-ups
     2.5 Min Total Run of the Vehicles.
   
Real time Dispatching of material flows in FMS embraces 11 cases
of current machine status:
       Waiting for workpiece;
       Machine Set-up period;
       Workpiece Processing;
       Tool offset and adjustment;
       Magnetic Cabinet failure;
       CNC-system failure;
       Mechanic Unit breakdown;
       Failure of Hydraulic Unit; 
       Main/Feed Drive Breakdown;
       Worktime losses due to organizational reasons;
       Switched off Machine.

 Any perturbations in the assigned production schedule occuring
for organizational  reasons  (lack of billets, tools, fixtures,
transport and logistics delays  etc.), as well as deviations in
certain  makespan of workpieces, cause changes  in  the current 
shift  plan  for some  machines.  Thus  initial schedule  falls
uneffective and then its further implementation  is undesirable 
because of an impending risk of production plan failure.
   The FMS rescheduling problem is NP-complete, and thus it
cannot be successfully  solved in real time  by means of simple 
scheduling policy. In the described program package "FOBOS" the 
Operation research problem  being regarded, efficient heuristic
algorithms for the described criteria are constructed.

At present software "FOBOS" runs under MS DOS for IBM PC AT/XT.
Available scheduling parameters:
       Number of Machines             -        150;
       Number of Lots of workpieces   - up to 3000;
       Length of technological routes
                 for each workpiece   -   16 stages.



     


